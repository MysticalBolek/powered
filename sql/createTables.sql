CREATE TABLE power_plant (
    id SERIAL NOT NULL,
    name varchar UNIQUE NOT NULL
);

CREATE TABLE power_plant_unit (
    id SERIAL NOT NULL,
    name VARCHAR,
    power_plant_id INTEGER NOT NULL,
    power REAL NOT NULL
);

CREATE TABLE power_loss (
    id SERIAL NOT NULL,
    power_plant_unit_id INTEGER NOT NULL,
    planned BOOL,
    start_date TIMESTAMP NOT NULL,
    end_date TIMESTAMP NOT NULL,
    loss_value REAL NOT NULL
);

CREATE TYPE type_of_day AS ENUM ('work_day', 'weekend_day', 'holiday');

CREATE TYPE season_type AS ENUM ('spring', 'summer', 'autumn', 'winter');

CREATE TABLE day (
    id SERIAL NOT NULL,
    date DATE NOT NULL,
    day_type type_of_day NOT NULL,
    season season_type NOT NULL
);

CREATE TABLE hour (
   id SERIAL NOT NULL,
   hour TIME NOT NULL,
   time_change BOOL NOT NULL
);

CREATE TABLE temperature (
    id SERIAL NOT NULL,
    day_id INTEGER NOT NULL,
    hour_id INTEGER NOT NULL,
    temperature REAL NOT NULL
);

CREATE TABLE rdn (
    id SERIAL NOT NULL,
    day_id INTEGER NOT NULL,
    hour_id INTEGER NOT NULL,
    fixing_1_price REAL,
    fixing_1_volume REAL,
    fixing_2_price REAL,
    fixing_2_volume REAL,
    continuous_price REAL,
    continuous_volume REAL
);

CREATE TABLE wind_power(
    id SERIAL NOT NULL,
    day_id INTEGER NOT NULL,
    hour_id INTEGER NOT NULL,
    power REAL
);

CREATE TABLE power_requirements(
    id SERIAL NOT NULL,
    day_id INTEGER NOT NULL,
    hour_id INTEGER NOT NULL,
    power REAL
);
