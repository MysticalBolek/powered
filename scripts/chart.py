import sys
import numpy
import csv
import matplotlib.pyplot as plt


#run python3 chart.py <path to file>

def loadData(path):
    csvFile = open(path, 'r')
    rows = csv.reader(csvFile, delimiter=';')
    real = []
    predicted = []
    next (rows, None)
    for row in rows:
        #first column contains date, not important in prediction
        real.append(float(row[3]))
        predicted.append(float(row[2]))
    csvFile.close()
    return (real, predicted)


path = sys.argv[1]

results = loadData(path)

plt.plot(results[0], label="real")
plt.plot(results[1], label="predicted")

plt.legend()
plt.show()
