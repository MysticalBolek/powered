import sys
import numpy
import csv
from keras.models import Sequential
from keras.layers import Dense
from keras.layers.core import Dropout

trainingDataPath = sys.argv[1]

testDataPath = sys.argv[2]

outputFilePath = sys.argv[3]

def loadData(path):
    csvFile = open(path, 'r')
    rows = csv.reader(csvFile, delimiter=';')
    dates = []
    inParam = []
    output = []
    for row in rows:
        #first column contains date, not important in prediction
        dates.append(row[0])
        inputRow = []
        #columns used in prediction
        for i in range(1, len(row)-1):
            inputRow.append(float(row[i]))
        inParam.append(inputRow)
        #last column contains data, which we predict
        output.append([float(row[len(row)-1])])
    csvFile.close()
    return (dates, inParam, output)


training = loadData(trainingDataPath);


inputVector = training[1]
outputVector = training[2]

inSize = len(training[1][0])

#creating net, is important to input_dim was the same as in loaded data

model = Sequential()
model.add(Dense(4, input_dim=inSize, init='uniform', activation='relu'))
model.add(Dropout(0.25))
model.add(Dense(8, init='uniform', activation='relu'))
model.add(Dropout(0.25))
model.add(Dense(1, init='uniform'))

'''
model = Sequential()
model.add(Dense(20, input_dim=inSize, init='uniform', activation='relu'))
model.add(Dense(60, init='uniform', activation='relu'))
model.add(Dense(40, init='uniform', activation='relu'))
model.add(Dense(1, init='uniform'))
'''
'''
model = Sequential()
model.add(Dense(20, input_dim=inSize, init='uniform', activation='relu'))
model.add(Dense(40, init='uniform', activation='relu'))
model.add(Dense(80, init='uniform', activation='relu'))
model.add(Dense(50, init='uniform', activation='relu'))
model.add(Dense(20, init='uniform', activation='relu'))
model.add(Dense(14, init='uniform', activation='relu'))
model.add(Dense(1, init='uniform'))
'''

model.compile(loss='mean_squared_error', optimizer='adam', metrics=['accuracy'])

model.fit(inputVector, outputVector, nb_epoch=50, batch_size=3)

testData = loadData(testDataPath);
dates = testData[0]
inputVector = testData[1]
outputVector = testData[2]

#predicting
predictions = model.predict(inputVector)

#creating ouptut
output = open(outputFilePath, 'w+');
output.write('date;hour;predicted deficit; real deficit; error\n')
for i in range(len(predictions)):
    error =  abs(outputVector[i][0] - predictions[i][0])
    output.write(dates[i] + ';' + str(inputVector[i][0]) + ';' + str(predictions[i][0]) + ';' + str(outputVector[i][0]) + ';' + str(error) + '\r\n')

output.close()




