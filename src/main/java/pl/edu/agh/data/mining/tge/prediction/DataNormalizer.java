package pl.edu.agh.data.mining.tge.prediction;

import pl.edu.agh.data.mining.tge.db.generated.enums.SeasonType;
import pl.edu.agh.data.mining.tge.db.generated.enums.TypeOfDay;

public final class DataNormalizer {

	private DataNormalizer() {
	}

	public static int getSeasonId(SeasonType seasonType) {
		switch (seasonType) {
		case spring:
			return 1;
		case summer:
			return 2;
		case autumn:
			return 3;
		case winter:
			return 4;
		default:
			return -1;
		}
	}

	public static int getDayTypeId(TypeOfDay typeOfDay) {
		switch (typeOfDay) {
		case holiday:
			return 1;
		case weekend_day:
			return 2;
		case work_day:
			return 3;
		default:
			return -1;
		}
	}
}
