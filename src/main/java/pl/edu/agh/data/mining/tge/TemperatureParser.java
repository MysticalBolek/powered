package pl.edu.agh.data.mining.tge;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.jooq.DSLContext;

import com.google.common.base.Strings;

import pl.edu.agh.data.mining.tge.db.DatabaseHelper;
import pl.edu.agh.data.mining.tge.db.generated.tables.records.TemperatureRecord;

public class TemperatureParser {

	private static final String TEMPERATURE_FILE_1_PATH = "src/main/resources/pl/edu/agh/data/mining/tge/t_11_15.txt";

	private static final String TEMPERATURE_FILE_2_PATH = "src/main/resources/pl/edu/agh/data/mining/tge/t_16.txt";

	private static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

	public static void main(String[] args) throws IOException, ParseException, SQLException {
		System.out.println("Processing first file.");
		parseFile(TEMPERATURE_FILE_1_PATH);
		System.out.println("Processing first file finshed \nProcessing second file.");
		parseFile(TEMPERATURE_FILE_2_PATH);
		System.out.println("Processing second file finshed.");
	}

	private static void parseFile(String filePath)
			throws FileNotFoundException, IOException, ParseException, SQLException {
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		List<TemperatureRecord> records = new LinkedList<>();
		int index = 1;
		try (Connection connection = DatabaseHelper.createConnection();
				DSLContext context = DatabaseHelper.createContext(connection)) {
			for (String line = reader.readLine(); line != null; line = reader.readLine()) {
				System.out.println("Parsing " + index + " line.");
				if (line.length() < 49) {
					continue;
				}
				Date date = parseDate(line);
				Float temperature = parseTemperature(line);
				TemperatureRecord record = new TemperatureRecord();
				record.setDayId(DateHelper.getOrCreateDate(new java.sql.Date(date.getTime()), context));
				record.setHourId(DateHelper.getOrCreateHour(getHour(date), false, context));
				record.setTemperature(temperature);

				records.add(record);
				index++;
			}
			DatabaseHelper.loadToDatabase(records);
		} finally {
			reader.close();
		}
	}

	private static Date parseDate(String line) throws ParseException {
		char[] year = new char[4];
		line.getChars(39, 43, year, 0);
		char[] month = new char[2];
		line.getChars(43, 45, month, 0);
		char[] day = new char[2];
		line.getChars(45, 47, day, 0);
		char[] hour = new char[2];
		line.getChars(47, 49, hour, 0);

		StringBuilder builder = new StringBuilder();
		builder.append(year);
		builder.append("-");
		builder.append(month);
		builder.append("-");
		builder.append(day);
		builder.append(" ");
		builder.append(hour);
		builder.append(":");
		builder.append("00:00");

		return format.parse(builder.toString());
	}

	private static int getHour(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		return calendar.get(Calendar.HOUR_OF_DAY);
	}

	private static Float parseTemperature(String line) {
		char[] temp = new char[7];
		line.getChars(49, line.length(), temp, 0);

		String tempString = String.valueOf(temp);
		if (Strings.isNullOrEmpty(tempString)) {
			return null;
		}

		return Float.valueOf(tempString);
	}
}
