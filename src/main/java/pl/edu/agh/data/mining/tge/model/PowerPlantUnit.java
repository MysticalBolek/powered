package pl.edu.agh.data.mining.tge.model;

import java.util.Objects;

import com.google.common.base.Preconditions;

public class PowerPlantUnit {
	
	private final String name;

	private final int power;

	private PowerPlant parentPlant;
	
	public PowerPlantUnit(String name, int power) {
		this.name = name;
		this.power = power;
	}

	public String getName() {
		return name;
	}

	public int getPower() {
		return power;
	}
	
	public void setParentPowerPlant(PowerPlant parent) {
		Preconditions.checkNotNull(parent, "Cannot set a null parent power plant");
		parentPlant = parent;
	}
	
	public PowerPlant gerParentPowerPlant() {
		return parentPlant;
	}
	
	public String getDescription() {
		if (parentPlant == null) {
			return "<empty unit name>";
		}
		return String.join(" ", parentPlant.getName(), name);
	}
	
    @Override
    public int hashCode() {
    	int parentHash = parentPlant != null ? parentPlant.hashCode() : 31;
    	return parentHash * name.hashCode() * Integer.hashCode(power);
    }
	
	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (other instanceof PowerPlantUnit) {
			return Objects.equals(this.parentPlant, ((PowerPlantUnit)other).parentPlant)
					&& Objects.equals(this.name, ((PowerPlantUnit)other).name)
					&& Objects.equals(this.power, ((PowerPlantUnit)other).power);
		}
		return false;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getDescription()).append("; power=").append(power);
		return builder.toString();		
	}
	
}
