package pl.edu.agh.data.mining.tge;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

public class DataLoader {

	public static void main(String[] args) throws FileNotFoundException, IOException, ParseException, SQLException {
		Crawler.main(args);
		RDNParser.main(args);
		TemperatureParser.main(args);
		WindPowerParser.main(args);
		PowerRequirementsParser.main(args);
	}
}
