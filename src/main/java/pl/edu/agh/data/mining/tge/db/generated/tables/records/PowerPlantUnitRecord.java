/**
 * This class is generated by jOOQ
 */
package pl.edu.agh.data.mining.tge.db.generated.tables.records;


import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record4;
import org.jooq.Row4;
import org.jooq.impl.TableRecordImpl;

import pl.edu.agh.data.mining.tge.db.generated.tables.PowerPlantUnit;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.8.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class PowerPlantUnitRecord extends TableRecordImpl<PowerPlantUnitRecord> implements Record4<Integer, String, Integer, Float> {

    private static final long serialVersionUID = 119770097;

    /**
     * Setter for <code>public.power_plant_unit.id</code>.
     */
    public void setId(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>public.power_plant_unit.id</code>.
     */
    public Integer getId() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>public.power_plant_unit.name</code>.
     */
    public void setName(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>public.power_plant_unit.name</code>.
     */
    public String getName() {
        return (String) get(1);
    }

    /**
     * Setter for <code>public.power_plant_unit.power_plant_id</code>.
     */
    public void setPowerPlantId(Integer value) {
        set(2, value);
    }

    /**
     * Getter for <code>public.power_plant_unit.power_plant_id</code>.
     */
    public Integer getPowerPlantId() {
        return (Integer) get(2);
    }

    /**
     * Setter for <code>public.power_plant_unit.power</code>.
     */
    public void setPower(Float value) {
        set(3, value);
    }

    /**
     * Getter for <code>public.power_plant_unit.power</code>.
     */
    public Float getPower() {
        return (Float) get(3);
    }

    // -------------------------------------------------------------------------
    // Record4 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row4<Integer, String, Integer, Float> fieldsRow() {
        return (Row4) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row4<Integer, String, Integer, Float> valuesRow() {
        return (Row4) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return PowerPlantUnit.POWER_PLANT_UNIT.ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return PowerPlantUnit.POWER_PLANT_UNIT.NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field3() {
        return PowerPlantUnit.POWER_PLANT_UNIT.POWER_PLANT_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Float> field4() {
        return PowerPlantUnit.POWER_PLANT_UNIT.POWER;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value3() {
        return getPowerPlantId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Float value4() {
        return getPower();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PowerPlantUnitRecord value1(Integer value) {
        setId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PowerPlantUnitRecord value2(String value) {
        setName(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PowerPlantUnitRecord value3(Integer value) {
        setPowerPlantId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PowerPlantUnitRecord value4(Float value) {
        setPower(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PowerPlantUnitRecord values(Integer value1, String value2, Integer value3, Float value4) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached PowerPlantUnitRecord
     */
    public PowerPlantUnitRecord() {
        super(PowerPlantUnit.POWER_PLANT_UNIT);
    }

    /**
     * Create a detached, initialised PowerPlantUnitRecord
     */
    public PowerPlantUnitRecord(Integer id, String name, Integer powerPlantId, Float power) {
        super(PowerPlantUnit.POWER_PLANT_UNIT);

        set(0, id);
        set(1, name);
        set(2, powerPlantId);
        set(3, power);
    }
}
