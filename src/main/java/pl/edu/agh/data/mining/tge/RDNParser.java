package pl.edu.agh.data.mining.tge;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.jooq.DSLContext;

import com.google.common.base.Strings;

import pl.edu.agh.data.mining.tge.db.DatabaseHelper;
import pl.edu.agh.data.mining.tge.db.generated.tables.records.RdnRecord;

public class RDNParser {

	private static final String FIRST_FILE_PATH = "src/main/resources/pl/edu/agh/data/mining/tge/RDN_1.CSV";

	private static final String SECOND_FILE_PATH = "src/main/resources/pl/edu/agh/data/mining/tge/RDN_2.CSV";

	public static void main(String[] args) throws FileNotFoundException, IOException, ParseException, SQLException {
		System.out.println("Processing first file.");
		parseFile(FIRST_FILE_PATH);
		System.out.println("Processing first file finished. \nProcessing second file.");
		parseFile(SECOND_FILE_PATH);
		System.out.println("Processing second file finished.");
	}

	private static void parseFile(String filePath)
			throws IOException, FileNotFoundException, ParseException, SQLException {
		List<RdnRecord> rdnRecords = new ArrayList<>();
		System.out.println("Pasing of " + filePath + " started");
		CSVParser parser = new CSVParser(new FileReader(filePath), CSVFormat.EXCEL.withDelimiter(';'));
		List<CSVRecord> records = parser.getRecords();
		Iterator<CSVRecord> iter = records.iterator();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		try (Connection connection = DatabaseHelper.createConnection();
				DSLContext context = DatabaseHelper.createContext(connection)) {
			int index = 1;
			while (iter.hasNext()) {
				CSVRecord volumeRecord = iter.next();
				// CSV file can have some junky lines so we omit them, first
				// line should have "Wolumen" as second value.
				if (volumeRecord.size() < 2 || !Objects.equals(volumeRecord.get(1), "Wolumen")) {
					continue;
				}
				// if next record do not exist report error.
				if (!iter.hasNext()) {
					throw new IllegalArgumentException("Wrong format of CSV file");
				}
				CSVRecord priceRecord = iter.next();
				// check if next record has "Cena" as second value, if not
				// repoert error.
				if (!Objects.equals(priceRecord.get(1), "Cena")) {
					throw new IllegalArgumentException("Wrong format of CSV file");
				}
				// if all requirements are met, process records
				processDay(dateFormat, context, rdnRecords, volumeRecord, priceRecord);
				System.out.println("Parsed " + index++ + " records.");
			}
		}
		parser.close();
		System.out.print("Parsing finished successfully.\nInserting into database...");
		DatabaseHelper.loadToDatabase(rdnRecords);
		System.out.println("Inserting finished successfully.");
	}

	/**
	 * Process records for volume and price for one day. Create RdnRecord for
	 * each day and add them to rdnRecords list.
	 * 
	 * @param dateFormat
	 * @param context
	 * @param rdnRecords
	 * @param volumeRecord
	 * @param priceRecord
	 * @throws SQLException
	 * @throws ParseException
	 */
	private static void processDay(SimpleDateFormat dateFormat, DSLContext context, List<RdnRecord> rdnRecords,
			CSVRecord volumeRecord, CSVRecord priceRecord) throws SQLException, ParseException {
		Date date = new Date(dateFormat.parse(volumeRecord.get(0)).getTime());
		int dayId = DateHelper.getOrCreateDate(date, context);
		int hoursInDay = 25;
		for (int i = 2; i < hoursInDay + 2; i++) {
			RdnRecord rdnRecord = new RdnRecord();
			rdnRecord.setDayId(dayId);
			int hour = (i - 2);
			boolean timeChange = false;
			if (hour == 2) {
				// handle 2a hour, which is used during changing time to winter
				timeChange = true;
			}
			if (hour >= 2) {
				hour--;
			}

			rdnRecord.setHourId(DateHelper.getOrCreateHour(hour, timeChange, context));
			// fixing 1
			rdnRecord.setFixing_1Volume(convertNumber(volumeRecord.get(i)));
			// fixing 2 is shifted 25 values from current position, because day
			// have 25 hours(with 2a hour)
			rdnRecord.setFixing_2Volume(convertNumber(volumeRecord.get(i + hoursInDay)));
			// continuous is shifted another 25 values from current position(50
			// actually), because day have 25 hours(with 2a hour)
			rdnRecord.setContinuousVolume(convertNumber(volumeRecord.get(i + 2 * hoursInDay)));

			// fixing 1
			rdnRecord.setFixing_1Price(convertNumber(priceRecord.get(i)));
			// fixing 2 is shifted 25 values from current position, because day
			// have 25 hours(with 2a hour)
			rdnRecord.setFixing_2Price(convertNumber(priceRecord.get(i + hoursInDay)));
			// continuous is shifted another 25 values from current position(50
			// actually), because day have 25 hours(with 2a hour)
			rdnRecord.setContinuousPrice(convertNumber(priceRecord.get(i + 2 * hoursInDay)));
			rdnRecords.add(rdnRecord);
		}
	}

	private static Float convertNumber(String string) {
		if (Strings.isNullOrEmpty(string)) {
			return null;
		}

		return Float.valueOf(string);
	}
}
