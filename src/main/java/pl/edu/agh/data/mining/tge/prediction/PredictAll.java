package pl.edu.agh.data.mining.tge.prediction;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class PredictAll {
	
	private static final DateFormat dateDormat = new SimpleDateFormat("yyyy-MM-dd");

	public static void main(String[] args) throws ParseException{
		
		Date trainingStartDate = new Date(dateDormat.parse("2015-01-01").getTime());
		Date trainingEndDate = new Date(dateDormat.parse("2016-06-30").getTime());
		Date testStartDate = new Date(dateDormat.parse("2016-08-01").getTime());
		Date testEndDate = new Date(dateDormat.parse("2016-09-01").getTime());
		Thread worker = new Thread(() -> {
			try {
				PowerRequirementsPredictor.predict(trainingStartDate, trainingEndDate, testStartDate, testEndDate);
			} catch (IOException | SQLException | InterruptedException e) {
				e.printStackTrace();
			}
		});
		worker.start();
		worker = new Thread(() -> {
			try {
				PowerDeficitPredictor.predict(trainingStartDate, trainingEndDate, testStartDate, testEndDate);
			} catch (IOException | SQLException | InterruptedException e) {
				e.printStackTrace();
			}
		});
		worker.start();
		worker = new Thread(() -> {
			try {
				PricePredictor.predict(trainingStartDate, trainingEndDate, testStartDate, testEndDate);
			} catch (IOException | SQLException | InterruptedException e) {
				e.printStackTrace();
			}
		});
		worker.start();
	}
}
