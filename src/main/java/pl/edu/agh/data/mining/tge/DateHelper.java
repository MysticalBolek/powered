package pl.edu.agh.data.mining.tge;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import org.jooq.DSLContext;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import pl.edu.agh.data.mining.tge.db.DatabaseHelper;
import pl.edu.agh.data.mining.tge.db.generated.enums.SeasonType;
import pl.edu.agh.data.mining.tge.db.generated.enums.TypeOfDay;
import pl.edu.agh.data.mining.tge.db.generated.tables.Day;
import pl.edu.agh.data.mining.tge.db.generated.tables.Hour;
import pl.edu.agh.data.mining.tge.db.generated.tables.records.DayRecord;
import pl.edu.agh.data.mining.tge.db.generated.tables.records.HourRecord;

public class DateHelper {

	private static Date SPRING_STAR;

	private static Date SUMMER_START;

	private static Date AUTUMN_START;

	private static Date WINTER_START;

	private static DateComparator dateComparator = new DateComparator();

	private static Multimap<Integer, Date> holidayMap = HashMultimap.create();

	// Create season start date and dates holidays in each year
	static {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			SPRING_STAR = new Date(format.parse("2016-03-21").getTime());
			SUMMER_START = new Date(format.parse("2016-06-22").getTime());
			AUTUMN_START = new Date(format.parse("2016-09-23").getTime());
			WINTER_START = new Date(format.parse("2016-12-22").getTime());

			List<Date> holidays = new ArrayList<>();
			fillConstantsHoliday(2011, holidays, format);
			holidays.add(new Date(format.parse("2011-03-24").getTime()));
			holidays.add(new Date(format.parse("2011-03-25").getTime()));
			holidays.add(new Date(format.parse("2011-06-12").getTime()));
			holidays.add(new Date(format.parse("2011-06-23").getTime()));
			holidayMap.putAll(2011, holidays);

			holidays = new ArrayList<>();
			fillConstantsHoliday(2012, holidays, format);
			holidays.add(new Date(format.parse("2012-04-08").getTime()));
			holidays.add(new Date(format.parse("2012-04-09").getTime()));
			holidays.add(new Date(format.parse("2012-05-27").getTime()));
			holidays.add(new Date(format.parse("2012-06-07").getTime()));
			holidayMap.putAll(2012, holidays);

			holidays = new ArrayList<>();
			fillConstantsHoliday(2013, holidays, format);
			holidays.add(new Date(format.parse("2013-03-31").getTime()));
			holidays.add(new Date(format.parse("2013-04-01").getTime()));
			holidays.add(new Date(format.parse("2013-05-19").getTime()));
			holidays.add(new Date(format.parse("2013-05-30").getTime()));
			holidayMap.putAll(2013, holidays);

			holidays = new ArrayList<>();
			fillConstantsHoliday(2014, holidays, format);
			holidays.add(new Date(format.parse("2014-04-20").getTime()));
			holidays.add(new Date(format.parse("2014-04-21").getTime()));
			holidays.add(new Date(format.parse("2014-06-08").getTime()));
			holidays.add(new Date(format.parse("2014-06-19").getTime()));
			holidayMap.putAll(2014, holidays);

			holidays = new ArrayList<>();
			fillConstantsHoliday(2015, holidays, format);
			holidays.add(new Date(format.parse("2015-04-05").getTime()));
			holidays.add(new Date(format.parse("2015-04-06").getTime()));
			holidays.add(new Date(format.parse("2015-05-24").getTime()));
			holidays.add(new Date(format.parse("2015-06-04").getTime()));
			holidayMap.putAll(2015, holidays);

			holidays = new ArrayList<>();
			fillConstantsHoliday(2016, holidays, format);
			holidays.add(new Date(format.parse("2016-03-27").getTime()));
			holidays.add(new Date(format.parse("2016-03-28").getTime()));
			holidays.add(new Date(format.parse("2016-05-15").getTime()));
			holidays.add(new Date(format.parse("2016-05-26").getTime()));
			holidayMap.putAll(2016, holidays);

		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public static int getOrCreateDate(Date date, DSLContext context) throws SQLException {
		List<Integer> ids = context.select().from(Day.DAY).where(Day.DAY.DATE.equal(date)).fetch(Day.DAY.ID);
		if (ids.isEmpty()) {
			DayRecord record = new DayRecord();
			record.setDate(date);
			record.setDayType(DateHelper.calculateDayType(date));
			record.setSeason(DateHelper.calculateSeason(date));
			DatabaseHelper.loadToDatabase(Arrays.asList(record));
			ids = context.select().from(Day.DAY).where(Day.DAY.DATE.equal(date)).fetch(Day.DAY.ID);
		}
		return ids.iterator().next();
	}

	public static int getOrCreateHour(int hour, boolean timeChange, DSLContext context) throws SQLException {
		List<Integer> ids = context.select().from(Hour.HOUR)
				.where(Hour.HOUR.HOUR_.eq(createTime(hour)).and(Hour.HOUR.TIME_CHANGE.eq(timeChange)))
				.fetch(Hour.HOUR.ID);
		if (ids.isEmpty()) {
			HourRecord record = new HourRecord();
			record.setTimeChange(timeChange);
			record.setHour(createTime(hour));
			DatabaseHelper.loadToDatabase(Arrays.asList(record));
			ids = context.select().from(Hour.HOUR).where(Hour.HOUR.HOUR_.eq(createTime(hour))).fetch(Hour.HOUR.ID);
		}

		return ids.iterator().next();
	}

	private static Time createTime(int hour) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, hour);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		return new Time(calendar.getTimeInMillis());
	}

	/**
	 * Fill constant holidays for year.
	 * 
	 * @param year
	 * @param listToFill
	 * @param format
	 * @throws ParseException
	 */
	private static void fillConstantsHoliday(int year, List<Date> listToFill, SimpleDateFormat format)
			throws ParseException {
		listToFill.add(new Date(format.parse(year + "-01-01").getTime()));
		listToFill.add(new Date(format.parse(year + "-01-06").getTime()));
		listToFill.add(new Date(format.parse(year + "-05-01").getTime()));
		listToFill.add(new Date(format.parse(year + "-05-03").getTime()));
		listToFill.add(new Date(format.parse(year + "-08-15").getTime()));
		listToFill.add(new Date(format.parse(year + "-11-01").getTime()));
		listToFill.add(new Date(format.parse(year + "-11-11").getTime()));
		listToFill.add(new Date(format.parse(year + "-12-25").getTime()));
		listToFill.add(new Date(format.parse(year + "-12-24").getTime()));
	}

	public static SeasonType calculateSeason(Date date) {
		Objects.requireNonNull(date);
		if (dateComparator.compare(date, SPRING_STAR) == -1) {
			return SeasonType.winter;
		}
		if (dateComparator.compare(date, SUMMER_START) == -1) {
			return SeasonType.spring;
		}
		if (dateComparator.compare(date, AUTUMN_START) == -1) {
			return SeasonType.summer;
		}
		if (dateComparator.compare(date, WINTER_START) == -1) {
			return SeasonType.autumn;
		}

		return SeasonType.winter;
	}

	public static TypeOfDay calculateDayType(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		Collection<Date> holidays = holidayMap.get(calendar.get(Calendar.YEAR));
		if (holidays.contains(date)) {
			return TypeOfDay.holiday;
		}
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		if (dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY) {
			return TypeOfDay.weekend_day;
		}
		return TypeOfDay.work_day;
	}

	private static class DateComparator implements Comparator<Date> {

		@Override
		public int compare(Date o1, Date o2) {
			Calendar date1 = Calendar.getInstance();
			date1.setTime(o1);
			Calendar date2 = Calendar.getInstance();
			date2.setTime(o2);
			if (date1.get(Calendar.MONTH) > date2.get(Calendar.MONTH)) {
				return 1;
			}
			if (date1.get(Calendar.MONTH) < date2.get(Calendar.MONTH)) {
				return -1;
			}
			if (date1.get(Calendar.DAY_OF_MONTH) > date2.get(Calendar.DAY_OF_MONTH)) {
				return 1;
			}
			if (date1.get(Calendar.DAY_OF_MONTH) < date2.get(Calendar.DAY_OF_MONTH)) {
				return -1;
			}
			return 0;
		}
	}
}
