package pl.edu.agh.data.mining.tge.prediction;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.Record2;
import org.jooq.Record3;
import org.jooq.Record4;
import org.jooq.Result;

import pl.edu.agh.data.mining.tge.db.DatabaseHelper;
import pl.edu.agh.data.mining.tge.db.generated.enums.SeasonType;
import pl.edu.agh.data.mining.tge.db.generated.enums.TypeOfDay;
import pl.edu.agh.data.mining.tge.db.generated.tables.Day;
import pl.edu.agh.data.mining.tge.db.generated.tables.Hour;
import pl.edu.agh.data.mining.tge.db.generated.tables.PowerLoss;
import pl.edu.agh.data.mining.tge.db.generated.tables.PowerRequirements;
import pl.edu.agh.data.mining.tge.db.generated.tables.Rdn;
import pl.edu.agh.data.mining.tge.db.generated.tables.Temperature;
import pl.edu.agh.data.mining.tge.db.generated.tables.WindPower;

public class DataProvider {

	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

	private static Calendar calendarA = Calendar.getInstance();

	private static Calendar calendarB = Calendar.getInstance();

	public static void savaPriceDataToFile(Date startDate, Date endDate, String filePath)
			throws IOException, SQLException {

		CSVFormat format = CSVFormat.DEFAULT.withDelimiter(';').withRecordSeparator('\n');

		try (Connection connection = DatabaseHelper.createConnection();
				DSLContext context = DatabaseHelper.createContext(connection);
				FileWriter fileWriter = new FileWriter(filePath);
				CSVPrinter printer = new CSVPrinter(fileWriter, format);) {
			Result<Record4<Integer, Date, SeasonType, TypeOfDay>> result = context
					.select(Day.DAY.ID, Day.DAY.DATE, Day.DAY.SEASON, Day.DAY.DAY_TYPE).from(Day.DAY)
					.where(Day.DAY.DATE.between(startDate, endDate)).fetch();

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(startDate);
			calendar.add(Calendar.DAY_OF_MONTH, -1);

			Result<Record1<Integer>> dayBefore = context.select(Day.DAY.ID).from(Day.DAY)
					.where(Day.DAY.DATE.eq(new Date(calendar.getTime().getTime()))).fetch();

		    List<Record4<Integer, Date, SeasonType, TypeOfDay>> daysReversed = new ArrayList<>();
			for (Record4<Integer, Date, SeasonType, TypeOfDay> record : result) {
				daysReversed.add(0, record);
			}
			
			int beforeDayId = dayBefore.iterator().next().get(Day.DAY.ID);
			for (Record4<Integer, Date, SeasonType, TypeOfDay> record : daysReversed) {
				int dayId = record.get(Day.DAY.ID);
				Result<Record2<Integer, Time>> hours = context
						.select(Hour.HOUR.ID, Hour.HOUR.HOUR_) //
						.from(Hour.HOUR) //
						.where(Hour.HOUR.TIME_CHANGE.eq(false)) //
						.fetch();
				for (Record2<Integer, Time> hour : hours) {
					int hourId = hour.get(Hour.HOUR.ID);
					calendar.setTime(hour.get(Hour.HOUR.HOUR_));
					List<Object> csvRecord = new ArrayList<>();
					csvRecord.add(DATE_FORMAT.format(record.get(Day.DAY.DATE)));
					csvRecord.add(calendar.get(Calendar.HOUR_OF_DAY));

					Result<Record1<Float>> temperature = context.select(Temperature.TEMPERATURE.TEMPERATURE_)
							.from(Temperature.TEMPERATURE) //
							.where(Temperature.TEMPERATURE.DAY_ID.equal(dayId)
									.and(Temperature.TEMPERATURE.HOUR_ID.equal(hourId)))
							.fetch();
					if (temperature.size() == 1) {
						float temperatureValue = temperature.iterator().next()
								.get(Temperature.TEMPERATURE.TEMPERATURE_);
						csvRecord.add(temperatureValue);
					} else {
						csvRecord.add(-100);
					}

					Result<Record1<Float>> powerRequirementRecord = context
							.select(PowerRequirements.POWER_REQUIREMENTS.POWER) //
							.from(PowerRequirements.POWER_REQUIREMENTS) //
							.where(PowerRequirements.POWER_REQUIREMENTS.DAY_ID.eq(beforeDayId))
							.and(PowerRequirements.POWER_REQUIREMENTS.HOUR_ID.eq(hourId)).fetch();
					if (powerRequirementRecord.size() == 1) {
						try {
							float powerRequirement = powerRequirementRecord.iterator().next()
									.get(PowerRequirements.POWER_REQUIREMENTS.POWER);
							csvRecord.add(powerRequirement);
						} catch (NullPointerException e) {
							csvRecord.add(-1);
						}
					} else {
						csvRecord.add(-1);
					}

					Result<Record1<Float>> lastDayRdnRecord = context.select(Rdn.RDN.FIXING_1_PRICE).from(Rdn.RDN)
							.where(Rdn.RDN.DAY_ID.equal(beforeDayId).and(Rdn.RDN.HOUR_ID.equal(hourId))).fetch();
					if (lastDayRdnRecord.size() == 1) {
						try {
							float fixing1Price = lastDayRdnRecord.iterator().next().get(Rdn.RDN.FIXING_1_PRICE);
							csvRecord.add(fixing1Price);
						} catch (NullPointerException e) {
							csvRecord.add(-1);
						}
					} else {
						csvRecord.add(-1);
					}

					Result<Record1<Float>> rdnRecord = context.select(Rdn.RDN.FIXING_1_PRICE).from(Rdn.RDN)
							.where(Rdn.RDN.DAY_ID.equal(dayId).and(Rdn.RDN.HOUR_ID.equal(hourId))).fetch();
					if (rdnRecord.size() == 1) {
						try {
							float fixing1Price = rdnRecord.iterator().next().get(Rdn.RDN.FIXING_1_PRICE);
							csvRecord.add(fixing1Price);
						} catch (NullPointerException e) {
							csvRecord.add(-1);
						}
					} else {
						csvRecord.add(-1);
					}

					printer.printRecord(csvRecord);
				}
				beforeDayId = dayId;
			}

		}
	}

	public static void savaPowerRequirementsDataToFile(Date startDate, Date endDate, String filePath)
			throws IOException, SQLException {

		CSVFormat format = CSVFormat.DEFAULT.withDelimiter(';').withRecordSeparator('\n');

		try (Connection connection = DatabaseHelper.createConnection();
				DSLContext context = DatabaseHelper.createContext(connection);
				FileWriter fileWriter = new FileWriter(filePath);
				CSVPrinter printer = new CSVPrinter(fileWriter, format);) {
			Result<Record4<Integer, Date, SeasonType, TypeOfDay>> result = context
					.select(Day.DAY.ID, Day.DAY.DATE, Day.DAY.SEASON, Day.DAY.DAY_TYPE).from(Day.DAY)
					.where(Day.DAY.DATE.between(startDate, endDate)).fetch();

			Calendar calendar = Calendar.getInstance();
			Result<Record3<Integer, Time, Boolean>> hours = context
					.select(Hour.HOUR.ID, Hour.HOUR.HOUR_, Hour.HOUR.TIME_CHANGE).from(Hour.HOUR).where(Hour.HOUR.TIME_CHANGE.eq(false)).fetch();
			
		    List<Record4<Integer, Date, SeasonType, TypeOfDay>> daysReversed = new ArrayList<>();
			for (Record4<Integer, Date, SeasonType, TypeOfDay> record : result) {
				daysReversed.add(0, record);
			}
			
			for (Record4<Integer, Date, SeasonType, TypeOfDay> record : daysReversed) {
				int dayId = record.get(Day.DAY.ID);
				for (Record3<Integer, Time, Boolean> hour : hours) {
					int hourId = hour.get(Hour.HOUR.ID);
					calendar.setTime(hour.get(Hour.HOUR.HOUR_));
					List<Object> csvRecord = new ArrayList<>();
					csvRecord.add(DATE_FORMAT.format(record.get(Day.DAY.DATE)));
					csvRecord.add(calendar.get(Calendar.HOUR_OF_DAY));
					csvRecord.add(DataNormalizer.getSeasonId(record.get(Day.DAY.SEASON)));
					csvRecord.add(DataNormalizer.getDayTypeId(record.get(Day.DAY.DAY_TYPE)));

					Result<Record1<Float>> temperature = context.select(Temperature.TEMPERATURE.TEMPERATURE_)
							.from(Temperature.TEMPERATURE) //
							.where(Temperature.TEMPERATURE.DAY_ID.equal(dayId)
									.and(Temperature.TEMPERATURE.HOUR_ID.equal(hourId)))
							.fetch();
					if (temperature.size() == 1) {
						float temperatureValue = temperature.iterator().next()
								.get(Temperature.TEMPERATURE.TEMPERATURE_);
						csvRecord.add(temperatureValue);
					} else {
						continue;
					}

					Result<Record1<Float>> power = context.select(PowerRequirements.POWER_REQUIREMENTS.POWER)//
							.from(PowerRequirements.POWER_REQUIREMENTS) //
							.where(PowerRequirements.POWER_REQUIREMENTS.DAY_ID.equal(dayId)//
									.and(PowerRequirements.POWER_REQUIREMENTS.HOUR_ID.equal(hourId))) //
							.fetch();

					if (power.size() == 1) {
						Float powerValue = power.iterator().next().get(PowerRequirements.POWER_REQUIREMENTS.POWER);
						if (powerValue != null) {
							csvRecord.add(powerValue);
						} else {
							continue;
						}
					} else {
						continue;
					}

					printer.printRecord(csvRecord);
				}
			}
		}
	}

	public static void savePowerDeficitToFile(Date startDate, Date endDate, String filePath)
			throws SQLException, IOException {
		CSVFormat format = CSVFormat.DEFAULT.withDelimiter(';').withRecordSeparator('\n');

		try (Connection connection = DatabaseHelper.createConnection();
				DSLContext context = DatabaseHelper.createContext(connection);
				FileWriter fileWriter = new FileWriter(filePath);
				CSVPrinter printer = new CSVPrinter(fileWriter, format);) {
			Result<Record4<Integer, Date, SeasonType, TypeOfDay>> days = context
					.select(Day.DAY.ID, Day.DAY.DATE, Day.DAY.SEASON, Day.DAY.DAY_TYPE).from(Day.DAY)
					.where(Day.DAY.DATE.between(startDate, endDate)).fetch();

			Result<Record3<Integer, Time, Boolean>> hours = context
					.select(Hour.HOUR.ID, Hour.HOUR.HOUR_, Hour.HOUR.TIME_CHANGE).from(Hour.HOUR).where(Hour.HOUR.TIME_CHANGE.eq(false)).fetch();
			
		    List<Record4<Integer, Date, SeasonType, TypeOfDay>> daysReversed = new ArrayList<>();
			for (Record4<Integer, Date, SeasonType, TypeOfDay> record : days) {
				daysReversed.add(0, record);
			}

			for (Record4<Integer, Date, SeasonType, TypeOfDay> record : daysReversed) {
				int dayId = record.get(Day.DAY.ID);
				for (Record3<Integer, Time, Boolean> hour : hours) {
					int hourId = hour.get(Hour.HOUR.ID);
					calendarA.setTime(record.get(Day.DAY.DATE));
					calendarB.setTime(hour.get(Hour.HOUR.HOUR_));
					calendarA.set(Calendar.HOUR_OF_DAY, calendarB.get(Calendar.HOUR_OF_DAY));
					calendarA.set(Calendar.MINUTE, 0);
					calendarA.set(Calendar.SECOND, 0);

					Timestamp start = new Timestamp(calendarA.getTime().getTime());

					calendarA.add(Calendar.HOUR, 1);

					Timestamp end = new Timestamp(calendarA.getTime().getTime());

					Result<Record1<Float>> powerLosses = context.select(PowerLoss.POWER_LOSS.LOSS_VALUE)
							.from(PowerLoss.POWER_LOSS) //
							.where(PowerLoss.POWER_LOSS.START_DATE.lessOrEqual(start)) //
							.and(PowerLoss.POWER_LOSS.END_DATE.greaterOrEqual(end)) //
							.fetch();
					float totalLoss = 0;
					for (Record1<Float> powerLoss : powerLosses) {
						totalLoss += powerLoss.get(PowerLoss.POWER_LOSS.LOSS_VALUE);
					}
					if (totalLoss > 0) {
						Result<Record1<Float>> windPower = context.select(WindPower.WIND_POWER.POWER)
								.from(WindPower.WIND_POWER) //
								.where(WindPower.WIND_POWER.DAY_ID.eq(dayId))
								.and(WindPower.WIND_POWER.HOUR_ID.eq(hourId)) //
								.fetch();
						if (windPower.size() == 1) {
							totalLoss -= windPower.iterator().next().get(WindPower.WIND_POWER.POWER);
							if (totalLoss < 0) {
								totalLoss = 0;
							}
						}else{
							continue;
						}
					}
					
					double temperature;
					
					Result<Record1<Float>> temperatureRecord = context.select(Temperature.TEMPERATURE.TEMPERATURE_)
							.from(Temperature.TEMPERATURE) //
							.where(Temperature.TEMPERATURE.DAY_ID.equal(dayId)
									.and(Temperature.TEMPERATURE.HOUR_ID.equal(hourId)))
							.fetch();
					if (temperatureRecord.size() == 1) {
						temperature = temperatureRecord.iterator().next()
								.get(Temperature.TEMPERATURE.TEMPERATURE_);
					} else {
						continue;
					}

					List<Object> csvRecord = new ArrayList<>();
					calendarA.setTime(hour.get(Hour.HOUR.HOUR_));
					csvRecord.add(DATE_FORMAT.format(record.get(Day.DAY.DATE)));
					csvRecord.add(calendarA.get(Calendar.HOUR_OF_DAY));
					csvRecord.add(DataNormalizer.getSeasonId(record.get(Day.DAY.SEASON)));
					csvRecord.add(DataNormalizer.getDayTypeId(record.get(Day.DAY.DAY_TYPE)));
					csvRecord.add(temperature);
					csvRecord.add(totalLoss);
					printer.printRecord(csvRecord);
				}
			}
		}
	}
}
