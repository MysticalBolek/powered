package pl.edu.agh.data.mining.tge.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.google.common.base.Preconditions;

public class PowerPlant {
	
	private final String name;
	
	private final List<PowerPlantUnit> units = new ArrayList<>();
	
	public PowerPlant(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public void addUnits(Collection<PowerPlantUnit> units) {
		for (PowerPlantUnit unit : units) {
			addUnit(unit);
		}
	}
	
	public void addUnit(PowerPlantUnit unit) {
		Preconditions.checkNotNull(unit, "Cannot add a null power plant unit");
		unit.setParentPowerPlant(this);
		units.add(unit);
	}

	public List<PowerPlantUnit> getUnits() {
		return Collections.unmodifiableList(units);
	}
	
	public int getTotalPower() {
		return units.stream().mapToInt(unit -> unit.getPower()).sum();
	}

}
