package pl.edu.agh.data.mining.tge.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Collection;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.TableRecord;
import org.jooq.impl.DSL;

public class DatabaseHelper {

	public static void loadToDatabase(Collection<? extends TableRecord<?>> records) {
		System.out.print("Inserting into database...");
		try (Connection conn = createConnection(); DSLContext create = DSL.using(conn, SQLDialect.POSTGRES);) {
			create.batchInsert(records).execute();
			System.out.println("Finished successfully.");
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println(e.getNextException());
		}
	}
	
	public static Connection createConnection() throws SQLException{
		String userName = "postgres";
		String password = "postgres";
		String url = "jdbc:postgresql://127.0.0.1:5432/tge_db";

		return DriverManager.getConnection(url, userName, password);
	}
	
	public static DSLContext createContext(Connection connection){
		return DSL.using(connection, SQLDialect.POSTGRES);
	}
}
