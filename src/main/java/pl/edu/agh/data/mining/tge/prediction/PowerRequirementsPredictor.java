package pl.edu.agh.data.mining.tge.prediction;

import java.io.File;
import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import java.sql.Date;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class PowerRequirementsPredictor {

	private static final String TRAINING_DATA_PATH = "./data/power_training.csv";

	private static final String TEST_DATA_PATH = "./data/power_test.csv";

	private static final String PREDICTION_PATH = "./prediction/power_prediction.csv";

	private static final String ERROR_FILE_PATH = "./prediction/power_error.txt";

	private static final String OUTPUT_FILE_PATH = "./prediction/power_out.txt";
	
	private static final String PREDICTOR_PATH = "./scripts/power_prediction.py";

	private static final DateFormat dateDormat = new SimpleDateFormat("yyyy-MM-dd");

	public static void main(String[] args) throws IOException, SQLException, InterruptedException, ParseException {
		Date trainingStartDate = new Date(dateDormat.parse("2015-01-01").getTime());
		Date trainingEndDate = new Date(dateDormat.parse("2016-06-30").getTime());
		Date testStartDate = new Date(dateDormat.parse("2016-08-01").getTime());
		Date testEndDate = new Date(dateDormat.parse("2016-09-01").getTime());

		predict(trainingStartDate, trainingEndDate, testStartDate, testEndDate);
	}

	public static void predict(Date trainingStartDate, Date trainingEndDate, Date testStartDate, Date testEndDate)
			throws IOException, SQLException, InterruptedException {

		System.out.print("Pulling data...");
		DataProvider.savaPowerRequirementsDataToFile(trainingStartDate, trainingEndDate, TRAINING_DATA_PATH);

		DataProvider.savaPowerRequirementsDataToFile(testStartDate, testEndDate, TEST_DATA_PATH);
		System.out.println("done.");

		System.out.println("Prediction..see " + OUTPUT_FILE_PATH + " file to monitor progress.");
		ProcessBuilder builder = new ProcessBuilder("python3", PREDICTOR_PATH, TRAINING_DATA_PATH,
				TEST_DATA_PATH, PREDICTION_PATH);
		//builder.inheritIO();
		builder.redirectError(Redirect.INHERIT);
		//builder.redirectError(new File(ERROR_FILE_PATH));
		builder.redirectOutput(new File(OUTPUT_FILE_PATH));
		//Process process = builder.start();
		//System.out.println(process.waitFor());
		//System.out.println("done.");
	}
}
