package pl.edu.agh.data.mining.tge;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.sql.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.jooq.DSLContext;

import pl.edu.agh.data.mining.tge.db.DatabaseHelper;
import pl.edu.agh.data.mining.tge.db.generated.tables.records.WindPowerRecord;

public class WindPowerParser {

	private static final String WIND_POWER_FILE_1_PATH = "src/main/resources/pl/edu/agh/data/mining/tge/WindPower.csv";

	private static final SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");

	public static void main(String[] args) throws FileNotFoundException, IOException, SQLException {
		List<WindPowerRecord> windPowerRecords = new LinkedList<>();

		CSVParser parser = new CSVParser(new FileReader(WIND_POWER_FILE_1_PATH), CSVFormat.EXCEL.withDelimiter(';'));
		List<CSVRecord> records = parser.getRecords();
		Iterator<CSVRecord> iter = records.iterator();
		System.out.println("Start parsing power from wind.");
		int index = 1;
		try (Connection connection = DatabaseHelper.createConnection();
				DSLContext context = DatabaseHelper.createContext(connection)) {
			while (iter.hasNext()) {
				CSVRecord csvRecord = iter.next();
				String value = csvRecord.get(3);
				//data row contains date at 3 index, if there is no date omit row
				try {
					Date date = new Date(format.parse(value).getTime());
					//at index 4 is power during first hour (from 00:00 to 01:00)
					for (int i = 4; i < 28; i++) {
						WindPowerRecord windRecord = new WindPowerRecord();
						windRecord.setDayId(DateHelper.getOrCreateDate(date, context));
						windRecord.setHourId(DateHelper.getOrCreateHour(i - 4, false, context));
						windRecord.setPower(Float.valueOf(csvRecord.get(i)));
						windPowerRecords.add(windRecord);
					}

				} catch (ParseException e) {
					continue;
				}
				System.out.println("Row " +  (index++) + " parsed successfully");
			}
		}
		parser.close();
		DatabaseHelper.loadToDatabase(windPowerRecords);
	}	
}
