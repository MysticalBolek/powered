package pl.edu.agh.data.mining.tge;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.jooq.DSLContext;

import com.google.common.base.Strings;

import pl.edu.agh.data.mining.tge.db.DatabaseHelper;
import pl.edu.agh.data.mining.tge.db.generated.tables.records.PowerRequirementsRecord;

public class PowerRequirementsParser {

	private static final String SOURCE_FILE = "src/main/resources/pl/edu/agh/data/mining/tge/zapotrzebowanie.csv";

	public static void main(String[] args) throws FileNotFoundException, IOException, SQLException {

		List<PowerRequirementsRecord> recordsToAdd = new ArrayList<>();

		CSVParser csvParser = new CSVParser(new FileReader(SOURCE_FILE), CSVFormat.EXCEL.withDelimiter(';'));

		List<CSVRecord> records = csvParser.getRecords();
		Iterator<CSVRecord> iter = records.iterator();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		System.out.println("Starting parsing: " + SOURCE_FILE);
		try (Connection connection = DatabaseHelper.createConnection();
				DSLContext context = DatabaseHelper.createContext(connection)) {
			int index = 1;
			while (iter.hasNext()) {
				CSVRecord csvRecord = iter.next();
				try {
					Date date = new Date(dateFormat.parse(csvRecord.get(0)).getTime());
					int dayId = DateHelper.getOrCreateDate(date, context);
					for (int i = 1; i < 25; i++) {
						PowerRequirementsRecord record = new PowerRequirementsRecord();
						record.setDayId(dayId);

						int hourId = DateHelper.getOrCreateHour(i - 1, false, context);
						record.setHourId(hourId);

						String powerString = csvRecord.get(i).replaceAll(" ", "");
						if (!Strings.isNullOrEmpty(powerString)) {
							float power = Float.valueOf(csvRecord.get(i).replaceAll(" ", ""));
							record.setPower(power);
						}

						recordsToAdd.add(record);
					}
					System.out.println("Parsed " + (index++) + " line.");

				} catch (ParseException e) {
					System.out.println("Parsed " + (index++) + " line.");
					continue;
				}
			}

		} finally {
			csvParser.close();
		}

		System.out.println("Inserting power requirements");
		DatabaseHelper.loadToDatabase(recordsToAdd);
		System.out.println("Done");
	}
}
