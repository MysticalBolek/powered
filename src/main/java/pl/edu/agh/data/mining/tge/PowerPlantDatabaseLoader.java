package pl.edu.agh.data.mining.tge;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.jooq.DSLContext;
import org.jooq.Record2;
import org.jooq.Result;

import pl.edu.agh.data.mining.tge.db.DatabaseHelper;
import pl.edu.agh.data.mining.tge.db.generated.tables.PowerPlant;
import pl.edu.agh.data.mining.tge.db.generated.tables.PowerPlantUnit;
import pl.edu.agh.data.mining.tge.db.generated.tables.records.PowerLossRecord;
import pl.edu.agh.data.mining.tge.db.generated.tables.records.PowerPlantRecord;
import pl.edu.agh.data.mining.tge.db.generated.tables.records.PowerPlantUnitRecord;
import pl.edu.agh.data.mining.tge.model.Dataset;

public class PowerPlantDatabaseLoader {

	public static void loadData(Dataset dataset) throws SQLException {
		loadToDatabasePowerPlants(dataset.getPowerPlants());
		loadToDatabasePowerPlantsUnits(dataset.getPowerPlants());
		for (pl.edu.agh.data.mining.tge.model.PowerPlant powerPlant : dataset.getPowerPlants()) {
			loadToDatabasePowerLoss(dataset, powerPlant);
		}
	}

	private static void loadToDatabasePowerPlants(List<pl.edu.agh.data.mining.tge.model.PowerPlant> powerPlants) {
		List<PowerPlantRecord> powerPlantRecords = powerPlants.stream().map(powerPlant -> {
			PowerPlantRecord record = new PowerPlantRecord();
			record.setName(powerPlant.getName());
			return record;
		}).collect(Collectors.toList());

		DatabaseHelper.loadToDatabase(powerPlantRecords);
	}

	private static void loadToDatabasePowerPlantsUnits(List<pl.edu.agh.data.mining.tge.model.PowerPlant> powerPlants)
			throws SQLException {
		Map<String, Integer> powerPlantIdMap = new HashMap<>();
		Result<Record2<Integer, String>> results;
		try (Connection connection = DatabaseHelper.createConnection();
				DSLContext context = DatabaseHelper.createContext(connection)) {
			results = context.select(PowerPlant.POWER_PLANT.ID, PowerPlant.POWER_PLANT.NAME)
					.from(PowerPlant.POWER_PLANT).fetch();
		}
		for (Record2<Integer, String> r : results) {
			powerPlantIdMap.put(r.value2(), r.value1());
		}

		powerPlants.forEach(powerPlant -> loadToDatabasePowerPlantUnits(powerPlant.getUnits(),
				powerPlantIdMap.get(powerPlant.getName())));
	}

	private static void loadToDatabasePowerPlantUnits(
			List<pl.edu.agh.data.mining.tge.model.PowerPlantUnit> powerPlantUnits, int powerPlantId) {
		List<PowerPlantUnitRecord> powerPlantRecords = powerPlantUnits.stream().map(powerPlantUnit -> {
			PowerPlantUnitRecord record = new PowerPlantUnitRecord();
			record.setName(powerPlantUnit.getName());
			record.setPower((float) powerPlantUnit.getPower());
			record.setPowerPlantId(powerPlantId);
			return record;
		}).collect(Collectors.toList());

		DatabaseHelper.loadToDatabase(powerPlantRecords);
	}

	private static void loadToDatabasePowerLoss(Dataset dataset, pl.edu.agh.data.mining.tge.model.PowerPlant powerPlant)
			throws SQLException {
		List<Integer> id;
		try (Connection connection = DatabaseHelper.createConnection();
				DSLContext context = DatabaseHelper.createContext(connection)) {
			id = context.select().from(PowerPlant.POWER_PLANT)
					.where(PowerPlant.POWER_PLANT.NAME.equal(powerPlant.getName())).fetch(PowerPlant.POWER_PLANT.ID);
		}
		for (pl.edu.agh.data.mining.tge.model.PowerPlantUnit unit : powerPlant.getUnits()) {
			loadToDatabasePowerLoss(dataset, unit, id.iterator().next());
		}
	}

	private static void loadToDatabasePowerLoss(Dataset dataset, pl.edu.agh.data.mining.tge.model.PowerPlantUnit unit,
			int powerPlantId) throws SQLException {
		final List<Integer> id = new ArrayList<>();
		try (Connection connection = DatabaseHelper.createConnection();
				DSLContext context = DatabaseHelper.createContext(connection)) {
			id.addAll(context.select().from(PowerPlantUnit.POWER_PLANT_UNIT)
					.where(PowerPlantUnit.POWER_PLANT_UNIT.NAME.equal(unit.getName()))
					.and(PowerPlantUnit.POWER_PLANT_UNIT.POWER_PLANT_ID.equal(powerPlantId))
					.fetch(PowerPlantUnit.POWER_PLANT_UNIT.ID));
		}
		List<PowerLossRecord> records = dataset.getUnitPowerLosses(unit).stream().map(powerLoss -> {
			PowerLossRecord record = new PowerLossRecord();
			record.setLossValue((float) powerLoss.getValue());
			record.setPlanned(powerLoss.isPlanned());
			record.setPowerPlantUnitId(id.iterator().next());
			record.setStartDate(new Timestamp(powerLoss.getStartDate().getTime()));
			record.setEndDate(new Timestamp(powerLoss.getEndDate().getTime()));
			return record;
		}).collect(Collectors.toList());

		DatabaseHelper.loadToDatabase(records);
	}
}
