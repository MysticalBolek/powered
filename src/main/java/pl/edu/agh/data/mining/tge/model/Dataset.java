package pl.edu.agh.data.mining.tge.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

public class Dataset {
	
	private List<PowerPlant> powerPlants = new ArrayList<>();
	
	private Multimap<PowerPlantUnit, PowerLoss> powerLossMap = HashMultimap.create();
	
	public Dataset() {
		// Nothing to do here
	}
	
	public List<PowerPlant> getPowerPlants() {
		return powerPlants; // modifiable list on purpose here
	}
	
	public void addPowerLosses(Collection<PowerLoss> powerLosses) {
		for (PowerLoss powerLoss : powerLosses) {
			addPowerLoss(powerLoss);
		}
	}
	
	public void addPowerLoss(PowerLoss powerLoss) {
		PowerPlantUnit unit = powerLoss.getUnit();
		powerLossMap.put(unit, powerLoss);
	}
	
	public Collection<PowerLoss> getUnitPowerLosses(PowerPlantUnit unit) {
		return powerLossMap.get(unit);
	}
	
	public int getPowerLossesCount() {
		return powerLossMap.values().size();
	}

}
