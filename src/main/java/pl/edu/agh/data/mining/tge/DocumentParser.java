package pl.edu.agh.data.mining.tge;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.common.base.Preconditions;

import pl.edu.agh.data.mining.tge.model.PowerLoss;
import pl.edu.agh.data.mining.tge.model.PowerPlant;
import pl.edu.agh.data.mining.tge.model.PowerPlantUnit;

public class DocumentParser {
	
	private static final String HTML_POWERPLANT_TABLE_SELECTOR = "tr.powerStation table";
	
	private static final String HTML_POWERPLANT_NAME_SELECTOR = "tr.visible td.col1a";
	
	private static final String HTML_POWERPLANT_TOTAL_POWER_SELECTOR = "tr.visible td.col3a";
	
	private static final String HTML_POWERPLANT_UNIT_SELECTOR = "tr.details";
	
	private static final String HTML_POWERPLANT_UNIT_NAME_SELECTOR = "td.col2a";
	
	private static final String HTML_POWERPLANT_UNIT_POWER_SELECTOR = "td.col3a";
	
	private static final String HTML_POWERLOSSES_TABLE_SELECTOR = "table.ubytkiMocy.ubytkiDetails tbody";
	
	private static final String HTML_POWERLOSS_TABLE_ROW_SELECTOR = "tr";
	
	private static final String HTML_POWERLOSS_TABLE_CELL_SELECTOR = "td";
	
	private static final String UNRECOGNIZED_STRUCTURE_MESSAGE = "Unrecognized structure of power loss description table!";
	
	private static final String POWERLOSS_DATE_FORMAT = "dd-MM-yyyy HH:mm";
	
	private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat(POWERLOSS_DATE_FORMAT);
	
	private Document jSoupDoc;
	
	public DocumentParser(Document doc) {
		this.jSoupDoc = doc;
	}
	
	public List<PowerPlant> extractPowerPlants() {
		final List<PowerPlant> plants = new ArrayList<>();
		
		Elements powerPlantTables = jSoupDoc.select(HTML_POWERPLANT_TABLE_SELECTOR);
		for (Element powerPlant : powerPlantTables) {
			String powerPlantName = powerPlant.select(HTML_POWERPLANT_NAME_SELECTOR).text();
			int powerPlantTotalPower = Integer.parseInt(powerPlant.select(HTML_POWERPLANT_TOTAL_POWER_SELECTOR).text());
			
			Elements powerPlantUnits = powerPlant.select(HTML_POWERPLANT_UNIT_SELECTOR);
			List<PowerPlantUnit> units = extractPowerPlantUnits(powerPlantUnits, powerPlantTotalPower);
			
			final PowerPlant plant = new PowerPlant(powerPlantName);
			plant.addUnits(units);
			plants.add(plant);
		}
		
		return plants;
	}
	
	private List<PowerPlantUnit> extractPowerPlantUnits(Elements powerPlantUnits, int unitsTotalPower) {
		final List<PowerPlantUnit> units = new ArrayList<>();

		for (Element powerPlantUnit : powerPlantUnits) {
			String unitName = powerPlantUnit.select(HTML_POWERPLANT_UNIT_NAME_SELECTOR).text();
			int unitPower = Integer.valueOf(powerPlantUnit.select(HTML_POWERPLANT_UNIT_POWER_SELECTOR).text());
			units.add(new PowerPlantUnit(unitName, unitPower));
		}

		int parsedUnitsTotalPower = units.stream().mapToInt(unit -> unit.getPower()).sum();
		if (parsedUnitsTotalPower != unitsTotalPower) {
			throw new RuntimeException("An error ocurred during parsing power plant units");
		}

		return units;
	}
	
	public List<PowerLoss> extractPowerLosses(Map<String, PowerPlantUnit> powerPlantUnitMap) throws ParseException {
		final List<PowerLoss> result = new ArrayList<>();
		
		Elements powerLossesTables = jSoupDoc.select(HTML_POWERLOSSES_TABLE_SELECTOR);
		for (Element powerLoss : powerLossesTables) {
			result.add(extractPowerLoss(powerLoss, powerPlantUnitMap));
		}
		
		return result;
	}
	
	private PowerLoss extractPowerLoss(Element powerLossTable, Map<String, PowerPlantUnit> powerPlantUnitMap) throws ParseException {
		Elements keyValueProperties = powerLossTable.select(HTML_POWERLOSS_TABLE_ROW_SELECTOR);
		Preconditions.checkArgument(keyValueProperties.size() == 7, UNRECOGNIZED_STRUCTURE_MESSAGE);
		
		final PowerPlantUnit unit = resolvePowerPlantUnit(keyValueProperties, powerPlantUnitMap);
		final int value = resolvePowerLossValue(keyValueProperties);
		final boolean planned = resolvePlannedPropertyValue(keyValueProperties);
		final Date startDate = resolveLossStartDate(keyValueProperties);
		final Date endDate = resolveLossEndDate(keyValueProperties);
		
		return new PowerLoss(unit, value, planned, startDate, endDate);
	}
	
	private PowerPlantUnit resolvePowerPlantUnit(Elements keyValueProperties, Map<String, PowerPlantUnit> powerPlantUnitMap) {
		// 0 - Jednostka
		checkStructure(keyValueProperties, 0, "Jednostka");
		String unitString = getPropertyValue(keyValueProperties, 0);
		return powerPlantUnitMap.get(unitString);
	}
	
	private void checkStructure(Elements keyValueProperties, int index, String expectedString) {
		Preconditions.checkArgument(Objects.equals(expectedString, getPropertyName(keyValueProperties, index)), UNRECOGNIZED_STRUCTURE_MESSAGE);
	}
	
	private String getPropertyName(Elements keyValueProperties, int index) {
		return keyValueProperties.get(index).select(HTML_POWERLOSS_TABLE_CELL_SELECTOR).get(0).text();
	}
	
	private String getPropertyValue(Elements keyValueProperties, int index) {
		return keyValueProperties.get(index).select(HTML_POWERLOSS_TABLE_CELL_SELECTOR).get(1).text();
	}
	
	private int resolvePowerLossValue(Elements keyValueProperties) {
		// 2 - Wielkość ubytku:
		checkStructure(keyValueProperties, 2, "Wielkość ubytku:");
		String powerLossValueString = getPropertyValue(keyValueProperties, 2);
		String trimmedString = powerLossValueString.trim();
		String stringToParse = trimmedString.substring(0, trimmedString.indexOf(" "));
		return Integer.parseInt(stringToParse);
	}
	
	private boolean resolvePlannedPropertyValue(Elements keyValueProperties) {
		// 3 - Typ zdarzenia:
		checkStructure(keyValueProperties, 3, "Typ zdarzenia:");
		String plannedPropertyString = getPropertyValue(keyValueProperties, 3);
		if (plannedPropertyString.trim().startsWith("Planowane")) {
			return true;
		} else if (plannedPropertyString.trim().startsWith("Nieplanowane")) {
			return false;
		} else {
			throw new RuntimeException(String.format("Unrecognized property value \"%s\"", plannedPropertyString.trim()));
		}
	}
	
	private Date resolveLossStartDate(Elements keyValueProperties) throws ParseException {
		// 4 - Początek zdarzenia:
		checkStructure(keyValueProperties, 4, "Początek zdarzenia:");
		String startDateString = getPropertyValue(keyValueProperties, 4);
		return DATE_FORMATTER.parse(startDateString);
	}
	
	private Date resolveLossEndDate(Elements keyValueProperties) throws ParseException {
		// 5 - Koniec zdarzenia:
		checkStructure(keyValueProperties, 5, "Koniec zdarzenia:");
		String endDateString = getPropertyValue(keyValueProperties, 5);
		return DATE_FORMATTER.parse(endDateString);
	}

}
