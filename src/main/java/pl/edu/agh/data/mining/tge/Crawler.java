package pl.edu.agh.data.mining.tge;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.google.common.base.Preconditions;

import javafx.util.Pair;
import pl.edu.agh.data.mining.tge.model.Dataset;
import pl.edu.agh.data.mining.tge.model.PowerLoss;
import pl.edu.agh.data.mining.tge.model.PowerPlant;
import pl.edu.agh.data.mining.tge.model.PowerPlantUnit;

public class Crawler {

	private static final int SOCKET_TIMEOUT_RETRIALS = 10;

	private static final LocalDate START_DATE = LocalDate.of(2013, Month.OCTOBER, 1);

	private static final LocalDate END_DATE = LocalDate.now();

	private static final int SCAN_INTERVAL = 14; // 14 days

	private static final String URL_CORE = "http://gpi.tge.pl/zestawienie-ubytkow?p_p_id=gpicalendar_WAR_gpicalendarportlet"
			+ "&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_count=1"
			+ "&_gpicalendar_WAR_gpicalendarportlet_direction=0&_gpicalendar_WAR_gpicalendarportlet_current=%s";

	public static void main(String[] args) throws IOException, ParseException {
		int iterations = (int) (ChronoUnit.DAYS.between(START_DATE, END_DATE) / SCAN_INTERVAL);

		System.out.println("Parsing power plant units...");
		final List<PowerPlant> powerPlants = resolvePowerPlants();
		final Map<String, PowerPlantUnit> powerPlantUnitMap = resolveUnitMap(powerPlants);
		System.out.println(String.format("Successfully parsed %d power plants with total unit count of %d!\n",
				powerPlants.size(), powerPlantUnitMap.keySet().size()));

		System.out.println("Starting power loss crawling process...\n");
		final Dataset dataset = new Dataset();
		dataset.getPowerPlants().addAll(powerPlants);

		LocalDate date = START_DATE;
		for (int i = 1; i <= iterations; i++) {
			System.out.print(String.format("Processing iteration %d out of %d... ", i, iterations));

			Document document = getDocument(date);
			DocumentParser parser = new DocumentParser(document);
			dataset.addPowerLosses(parser.extractPowerLosses(powerPlantUnitMap));

			System.out.println("Done!\n");
			date = date.plusDays(SCAN_INTERVAL);
		}

		System.out.print("Checking the interality of the parsed data... ");
		checkDataIntegrality(dataset, powerPlants);
		System.out.println("Done!\n");

		System.out.println(String.format("Crawler finished parsing! Successfully parsed %d power losses!",
				dataset.getPowerLossesCount()));

		try {
			PowerPlantDatabaseLoader.loadData(dataset);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private static List<PowerPlant> resolvePowerPlants() throws IOException {
		Document document = getDocument(START_DATE);
		DocumentParser parser = new DocumentParser(document);
		return parser.extractPowerPlants();
	}

	private static Map<String, PowerPlantUnit> resolveUnitMap(List<PowerPlant> powerPlants) {
		Map<String, PowerPlantUnit> map = new HashMap<>();

		powerPlants.stream().map(plant -> plant.getUnits()).flatMap(units -> units.stream())
				.forEach(unit -> map.put(unit.getDescription(), unit));

		return map;
	}

	private static Document getDocument(LocalDate targetDate) throws IOException {
		String date = String.format("%4d-%2d-%d", targetDate.getYear(), targetDate.getMonth().getValue(),
				targetDate.getDayOfMonth());
		String url = String.format(URL_CORE, date);

		int trialNumber = 0;
		Document resultDocument = null;
		while (resultDocument == null && trialNumber <= SOCKET_TIMEOUT_RETRIALS) {
			try {
				trialNumber++;
				resultDocument = Jsoup.connect(url).get();
			} catch (SocketTimeoutException e) {
				if (trialNumber == SOCKET_TIMEOUT_RETRIALS) {
					throw e;
				}
			}
		}

		return resultDocument;
	}

	private static void checkDataIntegrality(Dataset dataset, List<PowerPlant> powerPlants) {
		final List<PowerPlantUnit> units = powerPlants.stream().map(plant -> plant.getUnits())
				.flatMap(unitList -> unitList.stream()).collect(Collectors.toList());
		for (PowerPlantUnit unit : units) {
			Collection<PowerLoss> powerLosses = dataset.getUnitPowerLosses(unit);
			Set<Pair<Date, Date>> failureDates = new HashSet<>();
			for (PowerLoss powerLoss : powerLosses) {
				Preconditions.checkArgument(Objects.equals(unit, powerLoss.getUnit()),
						"Invalid source power unit reference");
				Pair<Date, Date> descriptor = new Pair<>(powerLoss.getStartDate(), powerLoss.getEndDate());
				if (failureDates.contains(descriptor)) {
					throw new RuntimeException("Duplicate failures detected!");
				}
				failureDates.add(descriptor);
			}
		}
	}

}
