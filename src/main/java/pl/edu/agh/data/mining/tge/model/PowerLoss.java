package pl.edu.agh.data.mining.tge.model;

import java.util.Date;
import java.util.Objects;

public class PowerLoss {
	
	private final PowerPlantUnit unit;
	
	private final int value;
	
	private final boolean planned;
	
	private final Date startDate;
	
	private final Date endDate;
	
	public PowerLoss(PowerPlantUnit unit, int value, boolean planned, Date startDate, Date endDate) {
		this.unit = unit;
		this.value = value;
		this.planned = planned;
		this.startDate = startDate;
		this.endDate = endDate;
	}
	
	public PowerPlantUnit getUnit() {
		return unit;
	}

	public int getValue() {
		return value;
	}

	public boolean isPlanned() {
		return planned;
	}
	
	public Date getStartDate() {
		return startDate;
	}
	
	public Date getEndDate() {
		return endDate;
	}
	
    @Override
    public int hashCode() {
    	return unit.hashCode() * Integer.hashCode(value) * Boolean.hashCode(planned) * startDate.hashCode() * endDate.hashCode();
    }
	
	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (other instanceof PowerLoss) {
			return Objects.equals(this.unit, ((PowerLoss)other).unit)
					&& Objects.equals(this.value, ((PowerLoss)other).value)
					&& Objects.equals(this.planned, ((PowerLoss)other).planned)
					&& Objects.equals(this.startDate, ((PowerLoss) other).startDate)
					&& Objects.equals(this.endDate, ((PowerLoss) other).endDate);
		}
		return false;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(unit.getDescription());
		builder.append("; loss=").append(value);
		builder.append("; planned=").append(planned);
		builder.append("; from=").append(startDate);
		builder.append("; to=").append(endDate);
		return builder.toString();
	}

}
